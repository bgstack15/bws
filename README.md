# README for bookmark web service
<!--
    Author: bgstack15
    Startdate: 2021-12-12
-->

BWS is a work-in-progress web app that centralizes my bookmarks. I'm not sure I'm ready for the limited sorting functionality of DataTables.

# Improve

* Drag-and-drop functionality is not completely implemented.
