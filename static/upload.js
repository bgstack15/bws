/* vim:set syntax=javascript ts=2 sw=2 sts=2 et: */
// copied from fuss.py
var ddup = {
  // (A) ON PAGE LOAD
  hzone: null, // HTML upload zone
  hstat: null, // HTML upload status
  hform: null, // HTML upload form
  hiddenfirst: null,
  init : function () {
    // (A1) GET HTML ELEMENTS
    ddup.hzone = document.getElementById("upzone");
    ddup.hstat = document.getElementById("upstat");
    ddup.hform = document.getElementById("upform");
    //ddup.hiddenfirst = document.getElementById("hiddenfirst");

    // (A2) DRAG-DROP SUPPORTED
    if (window.File && window.FileReader && window.FileList && window.Blob) {
      // HIGHLIGHT DROPZONE ON FILE HOVER
      ddup.hzone.addEventListener("dragenter", function (e) {
        e.preventDefault();
        e.stopPropagation();
        ddup.hzone.classList.add('highlight');
        ddup.hzone.innerHTML = "Release to upload";
      });
      ddup.hzone.addEventListener("dragleave", function (e) {
        e.preventDefault();
        e.stopPropagation();
        ddup.hzone.classList.remove('highlight');
        ddup.hzone.innerHTML = "Drop links here";
      });

      // DROP TO UPLOAD FILE
      ddup.hzone.addEventListener("dragover", function (e) {
        e.preventDefault();
        e.stopPropagation();
      });
      ddup.hzone.addEventListener("drop", function (e) {
        e.preventDefault();
        e.stopPropagation();
        ddup.hzone.classList.remove('highlight');
        //ddup.hiddenfirst.style.display = "block" ;
        console.log(`length ${e.dataTransfer.items.length}`);
        let items = e.dataTransfer.items;
        for (var i=0;i<items.length;i++) {
          // kind is always "string", so ignore it
          items[i].getAsString(function (s){
            storeContents(s);
          });
          console.log(`item[${i}] type:${items[i].type}`);
        };
        // need the timeout
        // only wait 100ms. Any entries that haven't gotten here yet will just be lost. We only care about the one type of entry anyway.
        var timeout3 = setInterval(function() {
          clearInterval(timeout3); // stop the timer
          // Order of preferred datatypes, best to worst: 30,text/x-moz-place 40,text/x-moz-url 50,text/html
          var has_useful = false;
          var useful_priority = 100;
          var useful_string = "";
          for (var j = 0; j < items.length; j++) {
            var j_type = items[j].type;
            var j_string = storageContents[j];
            if (j_type == "text/x-moz-place" && useful_priority > 30) {
              has_useful = true;
              useful_priority = 30;
              useful_type = j_type; useful_string = j_string;
              console.log(`Choosing at priority ${useful_priority} item[${j}] type=${j_type} string="${j_string}"`);
            } else if (j_type == "text/x-moz-url" && useful_priority > 40) {
              has_useful = true;
              useful_priority = 40;
              useful_type = j_type; useful_string = j_string;
              console.log(`Choosing at priority ${useful_priority} item[${j}] type=${j_type} string="${j_string}"`);
            } else if (j_type == "text/html" && useful_priority > 50) {
              has_useful = true;
              useful_priority = 50;
              useful_type = j_type; useful_string = j_string;
              console.log(`Choosing at priority ${useful_priority} item[${j}] type=${j_type} string="${j_string}"`);
            }
          }
          if (!has_useful) {
            console.log(`Unable to find useful string. Please inspect the following, to determine for which to add support.`)
            for (var j = 0; j < items.length; j++) {
              var j_type = items[j].type;
              var j_string = storageContents[j];
              console.log(`item[${j}] type=${j_type} string="${j_string}"`);
            }
          } else {
            var params = new FormData();
            params.append('type',useful_type);
            params.append('string',useful_string);
            var username = document.getElementById('userlist');
            if (username) {
              params.append('username',username.value);
            };
            console.log(`Proceeding with "${useful_string}"`);
            let x3 = new XMLHttpRequest();
            x3.open("POST", "/edit/new/");
            x3.setRequestHeader("draganddrop", true);
            x3.send(params)
            x3.onload = function() {
              ddup.hzone.classList.remove('processing');
              ddup.hzone.innerHTML = "Drop links here";
              if (x3.status != 201) {
                ddup.hzone.classList.add('invalid');
              } else {
                // so we got a 201, which is object created!
                ddup.hzone.innerHTML = x3.responseText;
              }
              ddup.reset_text_timer(1500);
              if (refreshButton){refreshButton();};
            }
          }
          storageContents = []; // must clean up
        }, 100);
        //if (e.dataTransfer.files.length > 0) {
        //  ddup.hzone.classList.add('processing');
        //  ddup.hzone.innerHTML = "Uploading...";
        //  ddup.queue(e.dataTransfer.files);
        //} else {
        //  ddup.hzone.classList.add('invalid');
        //  ddup.hzone.innerHTML = "Input is not recognized!";
        //  ddup.reset_text_timer(1500);
        //}
      });
    }

    // (A3) DRAG-DROP UPLOAD NOT SUPPORTED
    else {
      ddup.hzone.style.display = "none";
      ddup.hform.style.display = "block";
    }
  },

  // (B) UPLOAD QUEUE + HANDLER
  // NOTE: AJAX IS ASYNCHRONOUS
  // A QUEUE IS REQUIRED TO STOP SERVER FLOOD
  upqueue : [], // upload queue
  uplock : false, // currently uploading a file
  queue : function (files) {
    // FILE LIST INTO QUEUE
    for (let f of files) {
      // OPTIONAL - SHOW UPLOAD STATUS
      ddup.thisdiv = document.getElementById(f.name);
      if (!ddup.thisdiv) {
        // add new div with filename as id
        //ddup.hstat.innerHTML += `<div id="${f.name}">${f.name} - Added to queue</div>`;
      } else {
        // change contents of existing div
        //ddup.thisdiv.innerHTML = `${f.name} - Added to queue`;
      }
      // ADD TO QUEUE
      ddup.upqueue.push(f);
    }
    // GO!
    ddup.go();
  },

  // (C) AJAX UPLOAD
  go : function () { if (!ddup.uplock && ddup.upqueue.length!=0) {
    // (C1) QUEUE STATUS UPDATE
    ddup.uplock = true;

    // (C2) PLUCK OUT FIRST FILE IN QUEUE
    let thisfile = ddup.upqueue[0];
    ddup.upqueue.shift();

    // OPTIONAL - SHOW UPLOAD STATUS
    //ddup.thisdiv = document.getElementById(thisfile.name);
    //ddup.thisdiv.innerHTML = `${thisfile.name} - Upload started`;
    // at start of queue, change color
    ddup.hzone.classList.add('processing');
    ddup.hzone.innerHTML = "Submitting bookmark...";

    // (C3) UPLOAD DATA
    let data = new FormData();
    data.append('file', thisfile);
    // ADD MORE POST DATA IF YOU WANT
    // data.append("KEY", "VALUE");

    // (C4) AJAX REQUEST
    let xhr = new XMLHttpRequest();
    xhr.open("POST", "/customdroppoint/");
    xhr.setRequestHeader("lastModified", thisfile.lastModified/1000);
    xhr.onload = function () {
      // OPTIONAL - SHOW UPLOAD STATUS
      //ddup.thisdiv.innerHTML = `${thisfile.name} - ${this.response}`;
      // NEXT BETTER PLAYER!
      ddup.hzone.classList.remove('processing');
      ddup.hzone.innerHTML = "Drop links here";
      ddup.uplock = false;
      ddup.go();
    };
    xhr.onerror = function(evt){
      // OPTIONAL - SHOW UPLOAD STATUS
      //ddup.thisdiv.innerHTML = `${thisfile.name} - AJAX ERROR`;
      // NEXT BETTER PLAYER!
      ddup.hzone.classList.remove('processing');
      ddup.hzone.innerHTML = "Drop links here";
      ddup.uplock = false;
      ddup.go();
    };
    xhr.send(data);
  } },

  reset_text_timer : function (delay_ms) {
    setTimeout(function (){
      ddup.hzone.classList.remove('invalid');
      ddup.hzone.innerHTML = "Drop links here";
    }, delay_ms);
  }
};
var storageTypes = []
var storageContents = []
// storeContents gets called by GetAsString which is a callback and cannot access anything useful outside the one thing
function storeContents(s) {
  storageContents.push(s);
}
window.addEventListener("DOMContentLoaded", ddup.init);
