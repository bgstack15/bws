// Startdate: 2021-12-17 15:07
// Reference:
function initial(user, edit_url, computername, title, groups, admingroup) {

   // Load computer name if not provided
   if ("" == computername) {
      if ('computername' in localStorage) {
         let x = new XMLHttpRequest();
         x.open("POST","/view/set/computername/"+localStorage['computername']+"/",async=true);
         x.send();
         x.onload = function() {
            if (x.status != 200) {
               console.log("Warning: unable to update computer name to server-side session.");
            } else {
               let cn = document.getElementById('computername');
               cn.innerHTML = "Computer name: " + localStorage['computername'];
            }
         }
      }
   } else {
      // computer name is provided, so let's update localStorage
      localStorage.setItem('computername',computername);
   };

   // Load tab title if defined in localStorage
   // This one always has a default, so trust localStorage over what is provided.
   console.log(`At start, localstorage.title="${localStorage['title']}" and title="${title}"`);
   if ('title' in localStorage) {
      console.log(`Setting tab title from localstorage, "${localStorage.getItem('title')}"`);
      document.title = localStorage.getItem('title');
   } else {
      console.log(`Setting localstorage with value "${title}"`);
      localStorage.setItem('title',title);
   };
   if (localStorage['title'] != title) {
      let y = new XMLHttpRequest();
      console.log(`Requesting to update session title to "${localStorage['title']}"`);
      y.open("POST","/view/set/title/"+localStorage['title']+"/",async=true);
      y.send();
      y.onload = function() {
         if (y.status != 200) {
            console.log("Warning: unable to update tab title to server-side session.");
         };
      };
   };

   // Load bookmark list
   loadBookmarkList(user);

   // add the user drop-down if the loggedinuser is a member of group admins
   //groupsarray = groups.split(',');
   let groupsArray = [];
   for (var i=0; i < groups.length; i++) { groupsArray.push(groups[i]); };
   indexOfAdmins = groupsArray.indexOf(admingroup);
   if (indexOfAdmins != -1) {
      console.log(`This user is a member of admin group "${admingroup}". Enabling user drop-down.`);
      let u = document.getElementById('userselector');
      var d = document.createElement('select');
      d.setAttribute('id','userlist');
      let x2 = new XMLHttpRequest();
      x2.open("GET","/view/list/users/", true);
      x2.setRequestHeader('Accept', 'application/json');
      x2.send();
      x2.onload = function() {
         if (x2.status != 200) {
            console.log(`Error ${x2.status}: ${x2.statusText}`);
         } else {
            //console.log(`Got list of users "${x2.responseText}"`);
            var user_list = JSON.parse(x2.response);
            for (var i=0;i<user_list.length;i++) {
               d.options[i] = new Option(user_list[i].name,user_list[i].name);
               if (user_list[i].name==user) {
                  d.value = user_list[i].name;
               }
            }
         }
      };
      u.append(d);
      d.onchange = function(){changeUserList(d);};
   };
};

function changeUserList(select_list) {
   loadBookmarkList(select_list.value);
};
function refreshButton() {
   loadBookmarkList(localStorage.getItem('lastuserlist'));
};

function loadBookmarkList(user) {
   // Load bookmark list
   let a = document.getElementById('list')
   let xhr = new XMLHttpRequest();
   xhr.open("GET","/view/list/"+user+"/", true);
   xhr.setRequestHeader('Accept', 'application/html');
   xhr.send();
   xhr.onload = function() {
      if (xhr.status != 200) {
         console.log(`Error ${xhr.status}: ${xhr.statusText}`);
      } else {
         localStorage.setItem('lastuserlist',user);
         a.innerHTML = xhr.responseText;
         // 2021-12-18 21:53 trying to add datatables
         $(document).ready( function () {
            $('#bm_list').DataTable(
               {
                  //options to DataTables go here
                  pageLength: 50
               }
            );
         } );
      }
   };
};
