// 2021-12-21
let tag = document.getElementById("tag");
let tags = document.getElementById("tags");
let tagsh = document.getElementById("tags_hidden");
let editform = document.getElementById("edit");
let ts = document.getElementById("timestamp");
let t = document.getElementById('title');
let fd = document.getElementById('delete');
let fd_bid = document.getElementById('fd_bid');
// blink function depends on css above
function blink_once(item) {
   //var item = document.getElementById(item);
   console.log(`Want to blink ${item} with ${item.value}`);
   item.classList.add('fadeIn');
   // remove the class, after the delay, so that the item can blink again if necessary.
   setTimeout(function(){item.classList.remove('fadeIn')},1000);
}
function add_option() {
   let text = tag.value;
   if (text != "" && text.match('[0-9a-zA-Z]') ) {
      var optexists = document.getElementById("opt_"+text);
      if (optexists) {
         // it already exists
         blink_once(optexists);
         } else {
         // need to add it
         var opt = document.createElement('option');
         opt.value = text; opt.innerHTML = text; opt.id = "opt_" + text;
         tags.appendChild(opt);
         tag.value = "";
         return 0;
      }
   }
   return 1;
};
function remove_option() {
   let text = tag.value;
   let opt = document.getElementById("opt_"+text);
   if (opt) { opt.remove(); };
   console.log(`Want to remove item "${text}" from list?`);
};
// When the tags list selection is changed, update the value of the tag entry field
function set_option() {
   if (tags.selectedIndex != -1) {
      let text = tags[tags.selectedIndex].value;
      console.log(`text is "${text}"`);
      tag.value = text;
   }
}
// Behavior of the add and remove buttons
document.getElementById("add").onclick = function(){add_option()};
document.getElementById("remove").onclick = function(){remove_option()};
// Behavior of the tags list
tags.onclick = function(){set_option()};
tags.onchange = function(){set_option()};
tags.onfocus = function(){this.selectedIndex = -1};
// when press enter, run the "add" event.
tag.onkeypress = function(event,value) {
   if (13 == event.which) {
      event.preventDefault(); // this blocks the Enter key from submitting the form, for this field only!
      console.log("Enter was pressed!");
      if (0 == add_option()) {
         console.log("Successful addition!")
      } else {
         console.log(`failed to add "${document.getElementById("tag").value}".`);
      }
   };
}
function submitform() {
   // add all options from list to hidden field, so all the options get submitted
   let j="";
   for (var i=0; i<tags.options.length; i++) {
      j += tags.options[i].value + ",";
   };
   tagsh.value = j;
};
// By passing the path to the function, the javascript does not need to be parsed by jinja2, so eventually I can split this js to a separate file so the template parsing is faster.
function getNow(dest) {
   let x = new XMLHttpRequest();
   x.open("GET",dest);
   x.send();
   x.onload = function() {
      if (x.status != 200) { alert("Unable to get server current time"); }
      else { ts.value = x.responseText; }
   };
};
function deleteBookmark() {
   let r = confirm(`Are you sure you want to delete bookmark "${t.value}"?`);
   console.log(`Got result "${r}" for bookmark ${fd_bid.value}`)
   if (r) {fd.submit();};
}
